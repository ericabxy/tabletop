Tiny Dragon 1x1/1 Size +2 Move 8
HD 3d12 Str +0 Dex +0
Armor 14 (+12 size +2 natural)
Bite +5, 1d4

Small Dragon 1x1/1 Size +1 Move 8
HD 4d12 Str +1 Dex +0
Armor 16 (+11 size +5 natural)
Bite +6, 1d6+1

Medium Dragon 1x1/1 Size +0 Move 8
HD 7d12 Str +2 Dex +0
Armor 16 (+10 size +6 natural)
Bite +9, 1d8+2

Large Dragon 2x2/2 Size -1 Move 8
HD 10d12 Str +4 Dex +0
Armor 18 (+9 size +9 natural)
Bite +13, 2d6+4

Huge Dragon 3x3/3 Size -2 Move 8
HD 19d12 Str +8 Dex -1
Armor 26 (+8 size +18 natural)
Bite +25, 2d8+8

Gargantuan Dragon 4x4/4 Size -4 Move 8
HD 27d12 Str +12 Dex -2
Armor 32 (+6 size +26 natural)
Bite +35, 4d6+12

Colossal Dragon 6x6/6 Size -8 Move 8
HD 38d12 Str +16 Dex -3
Armor 40 (+2 size +38 natural)
Bite +46, 4d8+16
